
/*
Author: Able Sense Media
Web: ablesense.com
Date of creation: 2014/01/01
*/

var APP = (function () {
	var me = {},
		browser = {}

	/////////////////////////////////////////////////////////////////
	////////////////////// PRIVATE FUNCTIONS ////////////////////////
	/////////////////////////////////////////////////////////////////
		//private vars
		;

	function getSVGsupport() {
		return document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#BasicStructure", "1.1");
	}

	function getMQsupport() {
		return (typeof window.matchMedia == 'function');
	}

	function isTouch() {
		return 'ontouchstart' in window || 'onmsgesturechange' in window;
	}

	function getAnimationSupport() {
		var b = document.body || document.documentElement,
			s = b.style,
			p = 'animation';

		if (typeof s[p] == 'string') { return true; }

		// Tests for vendor specific prop
		var v = ['Moz', 'webkit', 'Webkit', 'Khtml', 'O', 'ms'];
		p = p.charAt(0).toUpperCase() + p.substr(1);

		for (var i=0; i<v.length; i++) {
			if (typeof s[v[i] + p] == 'string') { return true; }
		}

		return false;
	}

	function getFlexboxSupport() {
		var props = ['-webkit-flex', '-ms-flexbox', 'flex'],
			len = props.length,
			detect = document.createElement('div'),
			supported = false;

		while(len-- && !supported) {
			var val = props[len];
			detect.style.display = val;
			supported = (detect.style.display === val);
		}

		return supported;
	}

	function getNthChildSupport() {
		// selectorSupported lovingly lifted from the mad italian genius, Diego Perini
		// http://javascript.nwbox.com/CSSSupport/

		var support,
			sheet,
			doc = document,
			root = doc.documentElement,
			head = root.getElementsByTagName('head')[0],
			impl = doc.implementation || {
				hasFeature: function() {
					return false;
				}
			},
			selector = ':nth-child(2n+1)',
			link = doc.createElement("style");

		link.type = 'text/css';

		(head || root).insertBefore(link, (head || root).firstChild);

		sheet = link.sheet || link.styleSheet;

		if (!(sheet && selector)) return false;

		support = impl.hasFeature('CSS2', '') ?

		function(selector) {
			try {
				sheet.insertRule(selector + '{ }', 0);
				sheet.deleteRule(sheet.cssRules.length - 1);
			} catch (e) {
				return false;
			}
			return true;
		} : function(selector) {
			sheet.cssText = selector + ' { }';
			return sheet.cssText.length !== 0 && !(/unknown/i).test(sheet.cssText) && sheet.cssText.indexOf(selector) === 0;
		};

		return support(selector);
	}

	function getViewportSize() {
		browser.viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
		browser.viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
	}

	function debounce(func, wait, immediate) {
		var timeout;
		return function() {
			var context = this, args = arguments;
			clearTimeout(timeout);
			timeout = setTimeout(function() {
				timeout = null;
				if (!immediate) func.apply(context, args);
			}, wait);
			if (immediate && !timeout) func.apply(context, args);
		};
	}

	/////////////////////////////////////////////////////////////////
	////////////////////// PUBLIC FUNCTIONS /////////////////////////
	/////////////////////////////////////////////////////////////////

	me.cancelEvent = function(event) {
		if (event.preventDefault) {
			event.preventDefault();
		} else {
			event.returnValue = false;
		}
	};

	me.onResize = function(callback) {
		callback();

		$(window).on('resize', debounce(function() {
			callback();
		}, 200));
	};


	browser.supportsAnimation = getAnimationSupport();
	browser.supportsSVG = getSVGsupport();
	browser.supportsMQ = getMQsupport();
	browser.supportsNthChild = getNthChildSupport();
	browser.supportsFlexbox = getFlexboxSupport();
	browser.viewportSize = getViewportSize();
	browser.isTouch = isTouch();
	browser.viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
	browser.viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

	me.onResize(getViewportSize);

	me.browser = browser;

	return me;
})();

(function() {
	$('html').removeClass('no-js');

	if (!APP.browser.supportsSVG) {
		$('html').addClass('no-svg');

		$('img').each(	function(n){
			var src = n.src;
			n.src = src.replace('.svg', '.png');
		});
	}
	// doesn't work for ie7

	if (!APP.browser.supportsMQ) {
		var respond = document.createElement('script');
		respond.src = '/js/respond.js';
		document.body.appendChild(respond);
	}

	if (!APP.browser.supportsNthChild) {
		//Test for nth-child support and add .clearrow class when not supported
	}

	// Mobile Navigation
	var mobile_trigger = false,
		navigation = $('#navigation');
// Wrote this with Glenn
	APP.onResize(function() {
		if (APP.browser.viewportWidth < 768) {
			// Mobile
			if (!mobile_trigger) {
				// Use the class .hidden to remove the navigation for roughly 300ms
				// before applying the everything below.
				// and remember to remove it once excuted.
				navigation.addClass('hidden');
				mobile_trigger = $('<a class="mobileTrigger" href="#navigation">Navigation</a>').on('click', function(e) {
					APP.cancelEvent(e); // Cancels the default behaviour
					navigation.toggleClass('open');
				});
				mobile_close = $('<a class="mobileClose" href="#navigation">Close</a>');
				navigation.prepend(mobile_close);
				mobile_close.on('click', function(e) {
					APP.cancelEvent(e);
					navigation.removeClass('open');
				});
				navigation.parent().append(mobile_trigger);
			}
			navigation.addClass('mobile');
		} else {
			// Desktop
			navigation.removeClass('mobile');
		}
	});
// end of what was written with Glenn


	// Still belongs to the above code - sets a dely on the application of the class hidden
	// to the mobile navigation
	var navRemoveHidden = setTimeout(function (){
		navigation.removeClass('hidden');
	});
	// Sets the delay to 200ms
	navRemoveHidden = 200;

	// Parallax Scroll
	var parallax_breadcrumbs = document.getElementById('breadcrumbs'),
		parallax_header = document.getElementById('header'),
		parallax_main = document.getElementById('main'),
		parallax_aside = document.getElementById('aside');

	if (parallax_breadcrumbs && parallax_aside) {
		// Without this it causes an error on a page that doesn't contain these
		// elements, ie the home page.
		var baH = parallax_aside.offsetHeight + parallax_breadcrumbs.offsetHeight,
			mH = parallax_main.offsetHeight;
	}

	function parallaxScroll() {
		var  sY = window.scrollY + 1,
			sYR = (sY / Math.sqrt(sY) / 12 + 1), // scrollY Root
			sSR = - parseInt(sY / (sYR), 10); // scrollSpeedReduction

			// The exponent should be based off the elements height.
			parabolic = parseInt((Math.pow(sYR, 3.5) + sYR), 10);
			sSR += parabolic;
			if (sSR > 0) { sSR = 0; } // Could this could be ternary operation?

		if (parallax_breadcrumbs && parallax_aside) {
			// if main height is great than breadcrumbs and aside height together,
			// and also sSR is less than the difference between the two,
			// allow the top position to change.
			if (mH > baH && sSR <= mH - baH) {
				parallax_breadcrumbs.style.top = - sSR + "px";
				parallax_aside.style.top = - sSR + "px";
			}
		}
		// Should it be translateY instead? I only changed it because of the image slider.
		parallax_main.style.top = sSR + "px";
	}

	if (APP.browser.viewportWidth >= 1280) {
		// I don't want the setInterval to continue to fire.
		// Should be setTimeout... that self clears?
		window.onscroll = setInterval(parallaxScroll, 16.67);
	}
})();
